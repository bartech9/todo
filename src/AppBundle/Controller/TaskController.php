<?php

namespace AppBundle\Controller;

use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\Form\FormBuilder;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use AppBundle\Entity\Task;

class TaskController extends Controller
{
    
    /**
     * @Route("/", name="index")
     */
    public function indexAction(Request $request)
    {
        $tasks = $this->getDoctrine()->getRepository('AppBundle:Task')->findAll();
        
        $task = new Task();
        $task->setDate(new \DateTime(date("Y-m-d H:i:s")));
        
        $form = $this->createFormBuilder($task)->add('text', 'text')->add('save', 'submit', array('label' => 'Create task'))->getForm();
        
        $form->handleRequest($request);
        
        if ($form->isValid()) {
            
            $em = $this->getDoctrine()->getManager();
            $em->persist($task);
            $em->flush();
            
            return $this->redirectToRoute('index', array('added' => true));
        }
        
        return $this->render('index.html.twig', array('tasks' => $tasks,
            'form' => $form->createView(),
            'added' => $request->query->get('added'),
            'deleted' => $request->query->get('deleted'),
            'edited' => $request->query->get('edited')));
    }   
    
    /**
     * @Route("/delete/{id}", name="delete")
     */
    public function deleteAction($id)
    {
        $task = $this->getDoctrine()->getRepository('AppBundle:Task')->find($id);
        
        if (!$task) {
            throw $this->createNotFoundException(
                'No task found for id '.$id
            );
        }
        
        $em = $this->getDoctrine()->getManager();        
        $em->remove($task);
        $em->flush();
        
        return $this->redirectToRoute('index', array('deleted' => true));
    }
    
    /**
     * @Route("/edit/{id}", name="edit")
     */
    public function editAction($id, Request $request)
    {
        $task = $this->getDoctrine()->getRepository('AppBundle:Task')->find($id);
    
        if (!$task) {
            throw $this->createNotFoundException(
                'No task found for id '.$id
            );
        }
        
        $form = $this->createFormBuilder($task)->add('text', 'text')->add('save', 'submit', array('label' => 'Edit task'))->getForm();
    
        $form->handleRequest($request);
    
        if ($form->isValid()) {
        
            $em = $this->getDoctrine()->getManager();   
            $em->flush();
        
            return $this->redirectToRoute('index', array('edited' => true));
        }
    
        return $this->render('edit.html.twig', array('form' => $form->createView()));       
    }
}
